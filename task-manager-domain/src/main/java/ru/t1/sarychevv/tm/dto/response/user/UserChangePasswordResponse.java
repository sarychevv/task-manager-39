package ru.t1.sarychevv.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class UserChangePasswordResponse extends AbstractUserResponse {

    public UserChangePasswordResponse(@Nullable final User user) {
        super(user);
    }

    public UserChangePasswordResponse(@Nullable final String token) {
        super(token);
    }

}
