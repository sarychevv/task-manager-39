package ru.t1.sarychevv.tm.model;

import lombok.NoArgsConstructor;
import ru.t1.sarychevv.tm.api.model.IWBS;

@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements IWBS {

}
