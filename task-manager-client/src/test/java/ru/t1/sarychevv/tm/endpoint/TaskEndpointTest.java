package ru.t1.sarychevv.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sarychevv.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sarychevv.tm.api.endpoint.ITaskEndpoint;
import ru.t1.sarychevv.tm.dto.request.task.*;
import ru.t1.sarychevv.tm.dto.request.user.UserLoginRequest;
import ru.t1.sarychevv.tm.dto.response.task.TaskClearResponse;
import ru.t1.sarychevv.tm.dto.response.task.TaskGetByIndexResponse;
import ru.t1.sarychevv.tm.dto.response.task.TaskUpdateByIndexResponse;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.marker.SoapCategory;
import ru.t1.sarychevv.tm.model.Task;

import java.util.List;

public class TaskEndpointTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String token;

    @Before
    public void initTest() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin("admin");
        userLoginRequest.setPassword("admin");
        token = authEndpoint.login(userLoginRequest).getToken();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(token);
            taskCreateRequest.setName("Task-" + i);
            taskCreateRequest.setDescription("Description-" + i);
            taskEndpoint.createTask(taskCreateRequest);
        }

    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeTaskStatusById() {
        @NotNull TaskChangeStatusByIdRequest taskChangeStatusByIdRequest = new TaskChangeStatusByIdRequest(token);
        @NotNull TaskGetByIndexRequest taskGetByIndexRequest = new TaskGetByIndexRequest(token);
        taskGetByIndexRequest.setIndex(1);
        @NotNull TaskGetByIndexResponse taskGetByIndexResponse = taskEndpoint.getTaskByIndex(taskGetByIndexRequest);
        @NotNull String Id = taskGetByIndexResponse.getTask().getId();
        taskChangeStatusByIdRequest.setStatus(Status.COMPLETED);
        taskChangeStatusByIdRequest.setId(Id);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest).getTask().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeTaskStatusByIndex() {
        @NotNull TaskChangeStatusByIndexRequest taskChangeStatusByIndexRequest = new TaskChangeStatusByIndexRequest(token);
        taskChangeStatusByIndexRequest.setStatus(Status.COMPLETED);
        taskChangeStatusByIndexRequest.setIndex(2);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.changeTaskStatusByIndex(taskChangeStatusByIndexRequest).getTask().getStatus());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCreateTask() {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(token);
        taskCreateRequest.setName("Task");
        taskCreateRequest.setDescription("Description");
        taskEndpoint.createTask(taskCreateRequest);
        @NotNull final TaskGetByIndexRequest taskGetByIndexRequest = new TaskGetByIndexRequest(token);
        taskGetByIndexRequest.setIndex(NUMBER_OF_ENTRIES);
        Assert.assertEquals("Task", taskEndpoint.getTaskByIndex(taskGetByIndexRequest).getTask().getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetTaskById() {
        @NotNull TaskGetByIdRequest taskGetByIdRequest = new TaskGetByIdRequest(token);
        @NotNull TaskGetByIndexRequest taskGetByIndexRequest = new TaskGetByIndexRequest(token);
        taskGetByIndexRequest.setIndex(1);
        @NotNull TaskGetByIndexResponse taskGetByIndexResponse = taskEndpoint.getTaskByIndex(taskGetByIndexRequest);
        @NotNull String Id = taskGetByIndexResponse.getTask().getId();
        taskGetByIdRequest.setId(Id);
        Assert.assertNotNull(taskEndpoint.getTaskById(taskGetByIdRequest));
    }

    @Test
    @Category(SoapCategory.class)
    public void testListTask() {
        @NotNull final TaskListRequest taskListRequest = new TaskListRequest(token);
        Assert.assertNotNull(taskEndpoint.listTask(taskListRequest));
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveTaskByIndex() {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(token);
        taskCreateRequest.setName("Task-1");
        taskCreateRequest.setDescription("Description-1");
        taskEndpoint.createTask(taskCreateRequest);
        @NotNull final TaskRemoveByIndexRequest taskRemoveByIndexRequest = new TaskRemoveByIndexRequest(token);
        taskRemoveByIndexRequest.setIndex(NUMBER_OF_ENTRIES);
        Assert.assertEquals("Task-1", taskEndpoint.removeTaskByIndex(taskRemoveByIndexRequest).getTask().getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateTaskyIndex() {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(token);
        taskCreateRequest.setName("Task-2");
        taskCreateRequest.setDescription("Description-2");
        taskEndpoint.createTask(taskCreateRequest);
        @NotNull final TaskUpdateByIndexRequest taskUpdateByIndexRequest = new TaskUpdateByIndexRequest(token);
        taskCreateRequest.setName("Task test update name");
        taskCreateRequest.setDescription("Task test update description");
        @NotNull final TaskUpdateByIndexResponse taskUpdateByIndexResponse = taskEndpoint.updateTaskByIndex(taskUpdateByIndexRequest);
        Assert.assertEquals("Task test update name", taskUpdateByIndexResponse.getTask().getName());
        Assert.assertEquals("Task test update description", taskUpdateByIndexResponse.getTask().getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void testClearTasks() {
        @NotNull final TaskClearResponse taskClearResponse = taskEndpoint.clearTask(new TaskClearRequest(token));
        @Nullable List<Task> taskList = null;
        @NotNull TaskListRequest taskListRequest = new TaskListRequest(token);
        Assert.assertEquals(taskList, taskEndpoint.listTask(taskListRequest));
    }

}
