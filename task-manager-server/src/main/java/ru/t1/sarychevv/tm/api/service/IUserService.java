package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.model.User;

public interface IUserService {

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws Exception;

    @Nullable User removeByLogin(@Nullable String login) throws Exception;

    @Nullable
    User findByLogin(@Nullable String login) throws Exception;

    @Nullable
    User removeOne(@Nullable User model) throws Exception;

    @Nullable
    User setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @Nullable
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) throws Exception;

    @NotNull
    Boolean isLoginExist(@Nullable String login) throws Exception;

    @NotNull
    Boolean isEmailExist(@Nullable String email) throws Exception;

    @NotNull
    User lockUserByLogin(@Nullable String login) throws Exception;

    @NotNull
    User unlockUserByLogin(@Nullable String login) throws Exception;

    @NotNull
    User findOneById(@Nullable String userId);
}
