package ru.t1.sarychevv.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.endpoint.IProjectEndpoint;
import ru.t1.sarychevv.tm.api.service.IProjectService;
import ru.t1.sarychevv.tm.api.service.IServiceLocator;
import ru.t1.sarychevv.tm.dto.request.project.*;
import ru.t1.sarychevv.tm.dto.response.project.*;
import ru.t1.sarychevv.tm.enumerated.ProjectSort;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "ru.t1.sarychevv.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(@WebParam(name = REQUEST, partName = REQUEST)
                                                                   @NotNull ProjectChangeStatusByIdRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable Project project = new Project();
        try {
            project = getProjectService().changeStatusById(userId, id, status);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                                         @NotNull ProjectChangeStatusByIndexRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable Project project = new Project();
        try {
            project = getProjectService().changeStatusByIndex(userId, index, status);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectClearResponse clearProject(@WebParam(name = REQUEST, partName = REQUEST)
                                             @NotNull ProjectClearRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        try {
            getProjectService().removeAll(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ProjectClearResponse();
    }

    @NotNull
    @WebMethod
    public ProjectCreateResponse createProject(@WebParam(name = REQUEST, partName = REQUEST)
                                               @NotNull ProjectCreateRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable Project project = new Project();
        try {
            project = getProjectService().create(userId, name, description);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectGetByIdResponse getProjectById(@WebParam(name = REQUEST, partName = REQUEST)
                                                 @NotNull ProjectGetByIdRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable Project project = new Project();
        try {
            project = getProjectService().findOneById(userId, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ProjectGetByIdResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectGetByIndexResponse getProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                       @NotNull ProjectGetByIndexRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable Project project = new Project();
        try {
            project = getProjectService().findOneByIndex(userId, index);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ProjectGetByIndexResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectListResponse listProject(@WebParam(name = REQUEST, partName = REQUEST)
                                           @NotNull ProjectListRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final ProjectSort sort = request.getSort();
        @NotNull List<Project> projects = new ArrayList<>();
        try {
            projects = getProjectService().findAll(userId, sort.getComparator());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ProjectListResponse(projects);
    }

    @NotNull
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(@WebParam(name = REQUEST, partName = REQUEST)
                                                       @NotNull ProjectRemoveByIdRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable Project project = new Project();
        try {
            project = getProjectService().removeOneById(userId, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ProjectRemoveByIdResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectRemoveByIndexResponse removeProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                             @NotNull ProjectRemoveByIndexRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable Project project = new Project();
        try {
            project = getProjectService().removeOneByIndex(userId, index);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ProjectRemoveByIndexResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(@WebParam(name = REQUEST, partName = REQUEST)
                                                       @NotNull ProjectUpdateByIdRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable Project project = new Project();
        try {
            project = getProjectService().updateById(userId, id, name, description);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectUpdateByIndexResponse updateProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                             @NotNull ProjectUpdateByIndexRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable Project project = new Project();
        try {
            project = getProjectService().updateByIndex(userId, index, name, description);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ProjectUpdateByIndexResponse(project);
    }

}
