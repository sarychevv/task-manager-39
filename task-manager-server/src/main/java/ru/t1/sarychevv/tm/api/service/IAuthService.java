package ru.t1.sarychevv.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.Session;
import ru.t1.sarychevv.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email) throws Exception;

    String login(@Nullable String login, @Nullable String password) throws Exception;

    @SneakyThrows
    Session validateToken(@Nullable String token);

    void invalidate(Session session) throws Exception;

}
