package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.Session;

import java.util.Comparator;
import java.util.List;

public interface ISessionService {

    @Nullable
    Session add(@Nullable String userId, @Nullable Session model) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    List<Session> findAll(@Nullable String userId) throws Exception;

    @Nullable
    Session findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    Session findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    @Nullable
    Session removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    Session removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @Nullable
    Session removeOne(@Nullable Session model) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;
}
