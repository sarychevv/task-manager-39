package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService{

    @Nullable
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    @Nullable
    Task create(@Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    Task updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    Task changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws Exception;

    @Nullable
    Task changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable String userId) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable String userId, @Nullable Comparator<Task> comparator) throws Exception;

    @Nullable
    Task findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @Nullable
    Task removeOne(@Nullable String userId, @Nullable Task model) throws Exception;

    @Nullable
    Task removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    Task removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @Nullable
    Task add(@Nullable String userId, @Nullable Task model) throws Exception;
}
