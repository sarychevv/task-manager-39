package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.IProjectRepository;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService{

    @Nullable
    Project create(@Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    Project changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws Exception;

    @Nullable
    Project changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable String userId) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable String userId, @Nullable Comparator<Project> comparator) throws Exception;

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @Nullable
    Project removeOne(@Nullable String userId, @Nullable Project model) throws Exception;

    @Nullable
    Project removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    Project removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @Nullable
    Project add(@Nullable String userId, @Nullable Project model) throws Exception;

}
