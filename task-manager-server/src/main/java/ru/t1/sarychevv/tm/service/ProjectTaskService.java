package ru.t1.sarychevv.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.service.IProjectService;
import ru.t1.sarychevv.tm.api.service.IProjectTaskService;
import ru.t1.sarychevv.tm.api.service.ITaskService;
import ru.t1.sarychevv.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sarychevv.tm.exception.entity.TaskNotFoundException;
import ru.t1.sarychevv.tm.exception.field.ProjectIdEmptyException;
import ru.t1.sarychevv.tm.exception.field.TaskIdEmptyException;
import ru.t1.sarychevv.tm.exception.field.UserIdEmptyException;
import ru.t1.sarychevv.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;


    public ProjectTaskService(@NotNull final IProjectService projectService, @NotNull final ITaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    @Nullable
    public Task bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    @Nullable
    public Task unbindTaskFromProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final List<Task> tasks = taskService.findAllByProjectId(userId, projectId);
        if (tasks == null) throw new TaskNotFoundException();
        for (final Task task : tasks) taskService.removeOneById(userId, task.getId());
        projectService.removeOneById(userId, projectId);
    }

}
