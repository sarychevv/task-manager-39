package ru.t1.sarychevv.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sarychevv.tm.api.repository.ITaskRepository;
import ru.t1.sarychevv.tm.api.repository.IUserRepository;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.marker.DBCategory;
import ru.t1.sarychevv.tm.model.User;
import ru.t1.sarychevv.tm.service.ConnectionService;
import ru.t1.sarychevv.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;

@Category(DBCategory.class)
public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String TEST_LOGIN = "user";

    private static final String TEST_EMAIL = "user@yandex.ru";

    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final SqlSession sqlSession = connectionService.getSqlSession();

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);

    @Before
    public void initRepository() throws Exception {
        userList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull User user = new User();
            user.setLogin(TEST_LOGIN + i);
            user.setEmail("user" + i + "@yandex.ru");
            user.setRole(Role.USUAL);
            userRepository.add(user);
            userList.add(user);
        }
    }

    @After
    public void removeRepository() throws Exception {
        userRepository.removeAll();
    }

    @Test
    public void testFindOneByLogin() throws Exception {
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.findOneByLogin(TEST_LOGIN));
    }

    @Test
    public void testFindOneByEmail() throws Exception {
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.findOneByEmail(TEST_EMAIL));
    }

    @Test
    public void testAdd() throws Exception {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        @Nullable final User actualUser = userRepository.findOneById(user.getId());
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(TEST_LOGIN, actualUser.getLogin());
        Assert.assertEquals(TEST_EMAIL, actualUser.getEmail());
    }

    @Test
    public void testFindAll() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.findAll().size());
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull User user = new User();
        @NotNull final String wrongId = "501";
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.findOneById(user.getId()));
        Assert.assertNotEquals(user, userRepository.findOneById(wrongId));
    }

    @Test
    public void testFindOneByIndex() throws Exception {
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.findOneByIndex(NUMBER_OF_ENTRIES));
        Assert.assertNotEquals(user, userRepository.findOneByIndex(NUMBER_OF_ENTRIES - 1));
    }

    @Test
    public void testGetSize() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize().intValue());
        Assert.assertEquals(userList.size(), userRepository.getSize().intValue());
    }

    @Test
    public void testRemoveOne() throws Exception {
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.removeOne(user));
        Assert.assertNull(userRepository.findOneById(user.getId()));
    }

    @Test
    public void testRemoveOneById() throws Exception {
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.removeOneById(user.getId()));
        Assert.assertNull(userRepository.findOneById(user.getId()));
    }

    @Test
    public void testRemoveOneByIndex() throws Exception {
        @NotNull User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setEmail(TEST_EMAIL);
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.removeOneByIndex(NUMBER_OF_ENTRIES));
        Assert.assertNull(userRepository.findOneById(user.getId()));
    }
}
