package ru.t1.sarychevv.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sarychevv.tm.api.service.*;
import ru.t1.sarychevv.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sarychevv.tm.exception.entity.TaskNotFoundException;
import ru.t1.sarychevv.tm.exception.field.ProjectIdEmptyException;
import ru.t1.sarychevv.tm.exception.field.TaskIdEmptyException;
import ru.t1.sarychevv.tm.marker.DBCategory;
import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.model.Task;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(DBCategory.class)
public class ProjectTaskTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    IProjectTaskService projectTaskService;
    @NotNull
    private List<Project> projectList;
    @NotNull
    private ITaskService taskService;
    @NotNull
    private IProjectService projectService;

    @Before
    public void initRepository() throws Exception {
        projectList = new ArrayList<>();
        taskService = new TaskService(connectionService);
        projectService = new ProjectService(connectionService);
        projectTaskService = new ProjectTaskService(projectService, taskService);
        @NotNull final Project project = new Project();
        @NotNull final Project project2 = new Project();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull Task task = new Task();
            task.setName("task" + i);
            task.setDescription("description" + i);
            if (i <= 5) {
                task.setUserId(USER_ID_1);
                project.setUserId(USER_ID_1);
                task.setProjectId(project.getId());
                taskService.add(USER_ID_1, task);
            } else {
                task.setUserId(USER_ID_2);
                project2.setUserId(USER_ID_2);
                task.setProjectId(project2.getId());
                taskService.add(USER_ID_2, task);
            }
        }
        projectService.add(null, project);
        projectService.add(null, project2);
        projectList.add(project);
        projectList.add(project2);
    }

    @After
    public void removeRepository() throws Exception {
        projectService.removeAll(USER_ID_1);
        projectService.removeAll(USER_ID_2);
        projectService.removeAll(null);
        taskService.removeAll(USER_ID_1);
        taskService.removeAll(USER_ID_2);
        taskService.removeAll(null);
    }

    @Test
    public void testBindTaskToProject() throws Exception {
        @Nullable final Task task = taskService.add(USER_ID_1, new Task());
        if (task == null) return;
        task.setUserId(USER_ID_1);
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String taskId = task.getId();
        @NotNull final Task bindedTask = projectTaskService.bindTaskToProject(USER_ID_1, projectId, taskId);
        Assert.assertEquals(task, bindedTask);
        Assert.assertEquals(taskService.findOneById(USER_ID_1, taskId), bindedTask);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testBindTaskToProjectWithoutProjectId() throws Exception {
        @Nullable final Task task = taskService.add(USER_ID_1, new Task());
        if (task == null) return;
        task.setUserId(USER_ID_1);
        @NotNull final String taskId = task.getId();
        projectTaskService.bindTaskToProject(USER_ID_1, null, taskId);
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testBindTaskToProjectWithoutTaskId() throws Exception {
        @Nullable final Task task = taskService.add(USER_ID_1, new Task());
        if (task == null) return;
        task.setUserId(USER_ID_1);
        @NotNull final String projectId = projectList.get(0).getId();
        projectTaskService.bindTaskToProject(USER_ID_1, projectId, null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testBindTaskToProjectWithWrongTaskId() throws Exception {
        @Nullable final Task task = taskService.add(USER_ID_1, new Task());
        if (task == null) return;
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String taskId = task.getId();
        projectTaskService.bindTaskToProject(USER_ID_1, projectId, taskId);
        projectTaskService.bindTaskToProject(USER_ID_1, projectId, taskId);
    }

    @Test
    public void testRemoveByProjectId() throws Exception {
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String projectId2 = projectList.get(1).getId();
        projectTaskService.removeProjectById(USER_ID_1, projectId);
        projectTaskService.removeProjectById(USER_ID_2, projectId2);
        @Nullable final List<Task> tasks = taskService.findAllByProjectId(USER_ID_1, projectId);
        @Nullable final List<Task> tasks2 = taskService.findAllByProjectId(USER_ID_2, projectId);
        if (tasks == null || tasks2 == null) return;
        Assert.assertEquals(0, tasks.size());
        Assert.assertEquals(0, tasks2.size());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testRemoveByProjectIdWithoutProjectId() throws Exception {
        projectTaskService.removeProjectById(USER_ID_1, null);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testRemoveByProjectIdWithWrongProjectId() throws Exception {
        projectTaskService.removeProjectById(USER_ID_1, "NonExist");
    }

    @Test
    public void testUnbindTaskToProject() throws Exception {
        @Nullable final Task task = taskService.add(USER_ID_1, new Task());
        if (task == null) return;
        task.setUserId(USER_ID_1);
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String taskId = task.getId();
        task.setProjectId(projectId);
        projectTaskService.unbindTaskFromProject(USER_ID_1, projectId, taskId);
        Assert.assertNotEquals(task.getProjectId(), projectId);
        Assert.assertNull(task.getProjectId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testUnbindTaskToProjectWithoutProjectId() throws Exception {
        @Nullable final Task task = taskService.add(USER_ID_1, new Task());
        if (task == null) return;
        task.setUserId(USER_ID_1);
        @NotNull final String taskId = task.getId();
        projectTaskService.unbindTaskFromProject(USER_ID_1, null, taskId);
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testUnbindTaskToProjectWithoutTaskId() throws Exception {
        @Nullable final Task task = taskService.add(USER_ID_1, new Task());
        if (task == null) return;
        task.setUserId(USER_ID_1);
        @NotNull final String projectId = projectList.get(0).getId();
        projectTaskService.unbindTaskFromProject(USER_ID_1, projectId, null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUnbindTaskToProjectWithWrongTaskId() throws Exception {
        @Nullable final Task task = taskService.add(USER_ID_1, new Task());
        if (task == null) return;
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String taskId = task.getId();
        projectTaskService.unbindTaskFromProject(USER_ID_1, projectId, taskId);
    }


}
